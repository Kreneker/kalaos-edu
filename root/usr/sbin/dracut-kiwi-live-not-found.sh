#!/bin/bash
set -e
# kamarada/Linux-Kamarada-GNOME#49 - dracut: dracut module 'kiwi-live' cannot be found or installed.
rm /etc/dracut.conf.d/02-livecd.conf

exit 0 
