<?xml version="1.0" encoding="utf-8"?>
<image schemaversion="6.8" name="KalaOS-Edu-1.0">
    <description type="system">
        <author>Kavin_Tee</author>
        <contact>kevin_tee@pm.me</contact>
        <specification>
            KalaOS - Breaking myth and misconception, glimps into the new territory
        </specification>
    </description>
    <preferences>
        <type image="iso" primary="true" flags="overlay" hybrid="true" hybridpersistent_filesystem="ext4" hybridpersistent="true"/>
        <version>1.99.1</version>
        <packagemanager>zypper</packagemanager>
        <locale>en_US</locale>
        <keytable>us</keytable>
        <timezone>Asia/Bangkok</timezone>
        <rpm-excludedocs>true</rpm-excludedocs>
        <rpm-check-signatures>false</rpm-check-signatures>
        <bootsplash-theme>bgrt</bootsplash-theme>
        <bootloader-theme>openSUSE</bootloader-theme>
    </preferences>
    <preferences>
        <type image="vmx" filesystem="ext4" bootloader="grub2" kernelcmdline="splash" firmware="efi"/>
        <type image="oem" filesystem="ext4" initrd_system="dracut" installiso="true" bootloader="grub2" kernelcmdline="quiet" firmware="efi">
            <oemconfig>
                <oem-systemsize>4096000</oem-systemsize>
                <oem-swap>true</oem-swap>
                <oem-device-filter>/dev/ram</oem-device-filter>
                <oem-multipath-scan>false</oem-multipath-scan>
                <oem-unattended>false</oem-unattended>
            </oemconfig>
            <machine memory="512" guestOS="suse" HWversion="4">
                <vmdisk id="0" controller="ide"/>
                <vmnic driver="e1000" interface="0" mode="bridged"/>
            </machine>
        </type>
    </preferences>
    <users>
        <user password="1234" pwdformat="plain" home="/root" name="root" groups="root"/>
        <user password="1234" pwdformat="plain" home="/home/tux" name="tux" groups="tux"/>
    </users>
    <repository type="rpm-md" >
        <source path='obsrepositories:/'/>
    </repository>
   <repository type="yast2" alias="Main Repository" imageinclude="true">
        <source path="https://download.opensuse.org/distribution/leap/15.1/repo/oss/"/>
    </repository>
    <repository type="yast2" alias="Main Update Repository" imageinclude="true">
        <source path="https://download.opensuse.org/update/leap/15.1/oss/"/>
    </repository>
    <repository type="yast2" alias="openSUSE-Leap-Non-Oss" imageinclude="true" priority="99">
        <source path="https://download.opensuse.org/distribution/leap/15.1/repo/non-oss/"/>
    </repository>
    <repository type="yast2" alias="openSUSE-Leap-Non-Oss-Update" imageinclude="true" priority="99">
        <source path="https://download.opensuse.org/update/leap/15.1/non-oss/"/>
    </repository>
    <repository type="yast2" alias="Science" imageinclude="true" priority="90">
        <source path="https://download.opensuse.org/repositories/science/openSUSE_Leap_15.1/"/>
    </repository>
    <repository type="yast2" alias="MultiMedia" imageinclude="true" priority="100">
        <source path="https://download.opensuse.org/repositories/multimedia:/apps/openSUSE_Leap_15.1/"/>
    </repository>
    <repository type="yast2" alias="Graphics" imageinclude="true" priority="90">
        <source path="https://download.opensuse.org/repositories/graphics/openSUSE_Leap_15.1/"/>
    </repository>
    <repository type="yast2" alias="Packman" imageinclude="true" priority="90">
        <source path="https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_15.1/"/>
    </repository>
    <repository type="yast2" alias="Geo" imageinclude="true" priority="90">
        <source path="https://download.opensuse.org/repositories/Application:/Geo/openSUSE_Leap_15.1/"/>
    </repository>
    <repository type="rpm-md" alias="VSCodium" imageinclude="true" priority="99">
        <source path="https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/rpms/"/>
    </repository>
    <repository type="rpm-md" alias="NVIDIA" imageinclude="true" priority="99">
        <source path="https://download.nvidia.com/opensuse/leap/15.1/"/>
    </repository>
    <repository type="rpm-md" alias="Education" imageinclude="true" priority="90">
        <source path="https://download.opensuse.org/repositories/Education/openSUSE_Leap_15.1/"/>
    </repository>
    <repository type="rpm-md" alias="Games" imageinclude="true" priority="90">
        <source path="https://download.opensuse.org/repositories/games/openSUSE_Leap_15.1/"/>
    </repository>
    <packages type="image" patternType="plusRecommended">
        <namedCollection name="apparmor"/>
        <namedCollection name="base"/>    
        <namedCollection name="enhanced_base"/>    
        <namedCollection name="fonts"/>
        <namedCollection name="x11"/>  
        <namedCollection name="lxqt"/>  
        <namedCollection name="yast2_basis"/>
        <namedCollection name="laptop"/>
        <package name="lxqt-theme-openSUSE-default"/>
        <package name="lxqt-theme-openSUSE-leaper"/>
        <package name="lxqt-theme-openSUSE-light"/>
        <package name="lxqt-themes"/>
        <package name="Mesa"/>
        <package name="Mesa-32bit"/>
        <package name="libvulkan1"/>
        <package name="libvulkan_intel"/>
        <package name="libvulkan_radeon"/>
        <package name="libvulkan_radeon-32bit"/>
        <package name="Mesa-libva"/>
        <package name="libva-vdpau-driver"/>
        <package name="libvdpau1"/>
        <package name="libvdpau1-32bit"/>
        <package name="vlc-vdpau"/>
        <package name="desktop-data-openSUSE-extra"/>
        <package name="spectacle"/>
        <package name="discover"/>
        <package name="discover-backend-flatpak"/>
        <package name="yast2-qt-branding-openSUSE"/>
        <package name="yast2-control-center"/>
        <package name="yast2-theme-breeze"/>
        <package name="yast2-trans-th"/>
        <package name="autoyast2"/>
        <package name="yast2-apparmor"/>
        <package name="yast2-aduc"/>
        <package name="yast2-boot-server"/>
        <package name="yast2-buildtools"/>
        <package name="yast2-cluster"/>
        <package name="yast2-configuration-management"/>
        <package name="yast2-devtools"/>
        <package name="yast2-dhcp-server"/>
        <package name="yast2-dns-server"/>
        <package name="yast2-fcoe-client"/>
        <package name="yast2-firstboot"/>
        <package name="yast2-ftp-server"/>
        <package name="yast2-geo-cluster"/>
        <package name="yast2-gpmc"/>
        <package name="yast2-http-server"/>
        <package name="yast2-multipath"/>
        <package name="yast2-nfs-server"/>
        <package name="yast2-nis-server"/>
        <package name="yast2-online-update-configuration"/>
        <package name="yast2-samba-provision"/>
        <package name="yast2-scanner"/>
        <package name="yast2-sound"/>
        <package name="yast2-squid"/>
        <package name="yast2-usbauth"/>
        <package name="hplip"/>
        <package name="hplip-sane"/>
        <package name="hplip-hpijs"/>
        <package name="OpenPrintingPPDs"/>
        <package name="OpenPrintingPPDs-hpijs"/>
        <package name="OpenPrintingPPDs-postscript"/>
        <package name="OpenPrintingPPDs-hpijs"/>
        <package name="OpenPrintingPPDs-ghostscript"/>
        <package name="gutenprint"/>
        <package name="manufacturer-PPDs"/>
        <package name="splix"/>
        <package name="epson-inkjet-printer-escpr"/>
        <package name="live-add-yast-repos"/>
        <package name="calamares"/>
        <package name="libpwquality1"/>
        <package name="rpcbind"/>
        <package name="mono-core"/>
        <package name="java-11-openjdk"/>
        <package name="x11-video-nvidiaG05"/>
        <package name="hardinfo"/>
        <package name="kinfocenter5"/>
        <package name="htop"/>
        <package name="ksystemlog"/>
        <package name="filelight"/>
        <package name="libreoffice"/>
        <package name="libreoffice-gtk3"/>
        <package name="libreoffice-gnome"/>
        <package name="libreoffice-l10n-th"/>
        <package name="thai-fonts"/>
        <package name="libvirt"/>
        <package name="virt-manager"/>
        <package name="gwenview5"/>
        <package name="webcamoid"/>
        <package name="codium"/>
        <package name="MozillaFirefox"/>
        <package name="MozillaThunderbird"/>
        <package name="flatpak"/>
        <package name="torbrowser-launcher"/>
        <package name="uget"/>
        <package name="vlc"/>
        <package name="audacious"/>
        <package name="audacity"/>
        <package name="lmms"/>
        <package name="musescore"/>
        <package name="tuxguitar"/>
        <package name="handbrake-gtk"/>
        <package name="obs-studio"/>
        <package name="gnucash"/>
        <package name="kmymoney"/>
        <package name="simple-scan"/> 
        <package name="xournal"/>
        <package name="scribus"/>
        <package name="okular"/>
        <package name="calibre"/>
        <package name="deluge"/>
        <package name="filezilla"/>
        <package name="grsync"/>
        <package name="gimp"/>
        <package name="inkscape"/>
        <package name="mypaint"/>
        <package name="kdenlive"/>
        <package name="openshot-qt"/>
        <package name="blender"/>
        <package name="godot"/>
        <package name="synfigstudio"/>
        <package name="opentoonz"/>
        <package name="kicad"/>
        <package name="FreeCAD"/>
        <package name="librecad"/>
        <package name="librecad-parts"/>
        <package name="kstars"/>
        <package name="marble"/>
        <package name="kalzium"/>
        <package name="qgis"/>
        <package name="imagej"/>
        <package name="osptracker"/>
        <package name="labplot-kf5"/>
        <package name="octave"/>
        <package name="openfoam"/>
        <package name="paraview"/>
        <package name="gmsh"/>
        <package name="darktable"/>
        <package name="rawtherapee"/>
        <package name="digikam"/>
        <package name="gnuhealth"/>
        <package name="keepassxc"/>
        <package name="breeze5-icons"/>
        <package name="speedcrunch"/>
        <package name="imagewriter"/>
        <package name="gnome-disk-utility"/>
        <package name="k3b"/>
        <package name="testdisk"/>
        <package name="sqlite3"/>
        <package name="git-core"/>
        <package name="kate"/>
        <package name="git-core"/>
        <package name="psmisc"/>
        <package name="command-not-found"/>
        <package name="sudo"/>
        <package name="libsecret-1-0"/>
        <package name="python2-SecretStorage"/>
        <package name="iproute2"/>
        <package name="etherape"/>
        <package name="wireshark"/>
        <package name="squid"/>
        <package name="netdata"/>
        <package name="xorg-x11-server"/>
        <package name="fuse-exfat"/>
        <package name="exfat-utils"/>
        <package name="python"/>
        <package name="perl"/>
        <package name="grub2-branding-openSUSE"/>
        <package name="ifplugd"/>
        <package name="iputils"/>
        <package name="sensors"/>
        <package name="libQt5Sensors5"/>
        <package name="libsensors4"/>
        <package name="hddtemp"/>
        <package name="vim"/>
        <package name="grub2"/>
        <package name="grub2-i386-pc"/>
        <package name="grub2-x86_64-efi" arch="x86_64"/>
        <package name="syslinux"/>
        <package name="lvm2"/>
        <package name="udev"/>
        <package name="bluez-qt-udev"/>
        <package name="btrfsprogs-udev-rules"/>
        <package name="libgudev-1_0-0"/>
        <package name="libinput-udev"/>
        <package name="libmtp-udev"/>
        <package name="libudev1-32bit"/>
        <package name="python3-pyudev"/>
        <package name="udev-configure-printer"/>
        <package name='wpa_supplicant'/>
        <package name='wpa_supplicant-gui'/>
        <package name='shim'/>
        <package name='wireless-regdb'/>
        <package name="plymouth"/>
        <package name="plymouth-theme-bgrt"/>
        <package name="plymouth-dracut"/>
        <package name="plymouth-branding-openSUSE"/>
        <package name="fontconfig"/>
        <package name="fonts-config"/>
        <package name="tar"/>
        <package name="rar"/>
        <package name="unzip"/>
        <package name="parted"/>
        <package name="gparted"/>
        <package name="openssh"/>
        <package name="iproute2"/>
        <package name="less"/>
        <package name="bash-completion"/>
        <package name="dhcp-client"/>
        <package name="which"/>
        <package name="kernel-default"/>
        <package name="timezone"/>
    </packages>
    <packages type="image" patternType="onlyRequired">
        <package name="lame"/>
        <package name="libavdevice56"/>
        <package name="libavdevice58"/>
        <package name="vlc-codecs"/>
    </packages>
    <packages type="iso">
        <package name="gfxboot-branding-openSUSE"/>
        <package name="dracut-kiwi-live"/>
    </packages>
    <packages type="oem">
        <package name="gfxboot-branding-openSUSE"/>
        <package name="dracut-kiwi-oem-repart"/>
        <package name="dracut-kiwi-oem-dump"/>
    </packages>
    <packages type="bootstrap">
        <package name="udev"/>
        <package name="filesystem"/>
        <package name="glibc-locale"/>
        <package name="cracklib-dict-full"/>
        <package name="ca-certificates"/>
        <package name="ca-certificates-mozilla"/>
        <package name="openSUSE-release"/>
    </packages>
</image>
