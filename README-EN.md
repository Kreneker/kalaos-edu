# What is KalaOS?

KalaOS is an operating system based on Linux (like Windows Android macOS etc) but Linux is an OS that can be modified and distributed freely, therefore many linux distro exist.

KalaOS is a Linux distro that based in OpenSUSE(https://www.opensuse.org/), modified using KIWI https://doc.opensuse.org/projects/kiwi/doc/

# Why create KalaOS?

KalaOS is not that different from OpenSUSE, mostly added softwares and themes.

But KalaOS have 2 goals

* Become the base from building other distro, make KIWI easier to understand and start.
* Change other people minds that Linux lack of software, and hard to use in everyday life from lack of software. So KalaOS come with maximumlism in mind.
* Make beautiful and attractive interface as default to stop people from thinking that linux look old and outdated.

# Who maintain package and security patch in KalaOS?

Packages and security patches are from OpenSUSE Tumbleweed.

## Infos about KalaOS

* Based on OpenSUSE Tumbleweed 
* Package Manager - Zypper
* Desktop Environment - KDE Plasma 5
* Network manager - NetworkManager
* Default file system - EXT4
* Icon theme - Papirus
* Plasma theme - Aritim Dark

## What are these files and folders?

The files that you see (root, config.xml, config.sh) is not important to use KalaOS but it is use to build KalaOS, it is a source code.

root - anything you put to this folders will exist in live image ISO and be included in the installed system, good for adding files/themes/settings for end user.

config.xml - For adding softwares you want to include

config.sh - I use this to add keys and start/stop services in the system. Also specify what user get to automatically login.

To install KalaOS use kalaos-kde1.iso(not yet uploaded)

## Build KalaOS from source code

Install KIWI (for OpenSUSE)

```
sudo zypper in kiwi-tools python3-kiwi
```

Download config.sh, config.xml, root and put it together in one folder.

Navigate to that folder.

```
cd /files that u downloaded/
```
Start building ISO live image.
```
sudo kiwi-ng --type iso system build  --description /files that u downloaded/ --target-dir /anywhere/kalaos-kde1
```
### Error KiwiRootDirExists

```
[ ERROR   ]: 15:08:09 | KiwiRootDirExists: Root directory /anywhere/kalaos-kde1/build/image-root already exists
```
Simple, just change the name to kalaos-kde2

```
export TMPDIR=/home/anywhere/; sudo -E kiwi-ng --type iso system build  --description /files that u downloaded/ --target-dir /anywhere/kalaos-kde2
```

### Error KiwiInstallPhaseFailed

```
ERROR: 17:06:15 | KiwiInstallPhaseFailed: Systen package installation failed: No provider of 'Ardour' found
```
The software that we want to install (Ardour) is not in the repository in config.xml, add new repository in config.xml

```
<repository type="yast2" alias="MultiMedia" imageinclude="true" priority="100">
    <source path="https://download.opensuse.org/repositories/multimedia:/apps/openSUSE_Tumbleweed/"/>
    </repository>
```
### How big is KalaOS ISO?

Right now is about 5.0 GiB.

### How to install Live Image on real hardware?

KIWI can make installer by itself, but it is not modern looking so we will use Calamares.

But default Calamares setting does not work properly. Which KalaOS already fixed, this is what KalaOS did. Follow these modifications below.

Use Calamares to install Live Image https://github.com/calamares/calamares
Add
```
<package name="calamares"/> 
```
in config.xml

Add dracut-kiwi-live-not-found.sh into /files that u downloaded/root/sbin (refer to source code as example)

Add modules, qml, branding, settings.conf of calamares into /files that u downloaded/root/usr/share/calamares (refer to source code as example)

change shellprocess.conf in /files that u downloaded/root/usr/share/calamares/modules by adding 

```
dontChroot: false
timeout: 10
script:

    - "usr/sbin/dracut-kiwi-live-not-found.sh"
```

in settings.conf then add - shellprocess before - dracut (refer to source code as example)

### Warning

If to many software is added into config.xml kiwi-ng will use alot of /tmp (KalaOS use ~15GiB) which could make /root disk full.

But we can tell kiwi-ng to use /home/anywhere/ as temp folder by using environment variable.

```
export TMPDIR=/home/anywhere/; sudo -E kiwi-ng --type iso system build  --description /files that u downloaded/ --target-dir /anywhere/kalaos-kde1
```
# What KalaOS source code do to make a working system? 

KalaOS source code is a modified version of existing template from https://github.com/OSInside/kiwi-descriptions

The major changes that make KalaOS work out-of-the-box is listed below.

### How KalaOS deal with package manager to ask if I trust the repositories everytime.

Go to repository that you want to add.

Manually download .key or .gpg file of that repo.

Put the key files in /root/keys (refer to source code as example)

Edit config.sh to include your key with 
```
rpm --import /keys/nvidia.key
```
(refer to source code as example)

### How KalaOS deal with no internet connection?

If NetworkManager is installed, Wicked must be disable.

In config.sh disable Wicked with 

```
baseRemoveService wicked
baseRemoveService wickedd
baseRemoveService wickedd-auto4
baseRemoveService wickedd-dhcp4
baseRemoveService wickedd-dhcp5
baseRemoveService wickedd-nanny
```

and enable NetworkManager with 

```
baseInsertService ModemManager
baseInsertService NetworkManager
```
(refer to source code as example)

### How KalaOS deal with plugin devices?

Install udev package in config.xml.

```
<package name="udev"/> 
```
Without udev, new devices that was plugin won't be regcognized properly.

### How KalaOS detect Wifi USB dongle properly?

There is a file called "70-persistent-net.rules" in "root/etc/udev/rules.d", which it change the name the interface name of the Wifi dongle which interfere with NetworkManager, causing Wifi dongle to not detect properly.

KalaOS source code delete 70-persistent-net.rules file, the worst case for deleting this file is that the interface name might be different, it is safe to delete the file.

### How KalaOS set a default theme?

KalaOS use Aritim theme as default, the theme can be manually installed in /root/home/tux/.local/share/plasma/ and /root/etc/skel/.local/share/plasma/ folder in KIWI descriptions. 

The theme is set by /root/home/tux/.config/plasmarc with "name=Aritim-Dark"

Default SDDM theme is Chili and can also be placed in /root/usr/share/sddm/themes/ with theme set by /root/etc/sddm.conf with "Current=plasma-chili"

Wallpaper config is here: /root/home/tux/.config/plasma-org.kde.plasma.desktop-appletsrc with wallpaper set by Image=file:///usr/share/wallpapers/KalaPenguin5brown.png

### Make image for Ubuntu, Debian, Fedora, CentOS, Mageia.

Checkout config.xml and config.sh here: 
```
https://github.com/OSInside/kiwi-descriptions
```

## Problems/Bugs/Softwarebugs/TODO

* How to properly transfer desktop theme to newly create user?

* Btrfs snapper integration on grub not support on Calamares at the moment

* Ardour need GTK2_RC_FILES variable  on desktop icon to start at the moment.

* Scilab Xcos is not support on Java 10 for OpenSUSE, replace with appimage at the moment. https://bugzilla.opensuse.org/show_bug.cgi?id=1153256 

* Karaoke, type writing software.

* Avatar icon for new user is generate with .icon and .icon.face from /etc/skel at the moment and doesn't take effect at first user creation, to change avatar, try to change avatar again.

## By Kevin Teenakul

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Special Thanks

* schaefi (@schaefi:matrix.org)
* deivit (@deivit:matrix.org)
* Conan Kudo (@conan_kudo:matrix.org)
* defolos @defolos:matrix.org
* and many others that help me

## Pre-installed softwares with KalaOS

Default password - 1234

Change keyboard layout - meta + spacebar

System Settings - ตั้งค่าหน้าตา 

YaST- ตั้งค่าระบบ http://yast.opensuse.org/

YaST Package Managment - ติดตั้งโปรแกรมขั้นสูง http://yast.opensuse.org/ 

Discover - ติดตั้งโปรแกรม https://userbase.kde.org/Discover

Firefox - เล่นเน็ต https://www.mozilla.org/
```
    -Ublock Origin
    -Decentraleye
    -Facebook Container
    -Uget Integration
    -Redirect AMP to HTML
    -DownThemAll
    -HTTPS Everywhere
    -Privacy Badger
```
LibreOffice - พิมพ์งาน https://www.libreoffice.org/
```
    -LibreOffice Writer - คล้าย MS Word
    -LibreOffice Impress - คล้าย MS PowerPoint
    -LibreOffice Calc - คล้าย MS Excel
    -LibreOffice Draw - คล้าย MS Visio
    -LibreOffice Base - คล้าย MS Access
```
Tor Browser - ใช้งานอินเตอร์เน็ตแบบนิรนามหรือการเข้า Deepweb https://www.torproject.org/

Steam - เกม https://store.steampowered.com/

Wine - ใช้เล่นโปรแกรม Windows บน Linux

Lutris - จัดการคลังเกมรวมถึงเกมที่ต้องใช้ Wine เล่น https://lutris.net/

Blender - อนิเมชั่น 3D https://www.blender.org/

FreeCAD - ออกแบบรูปทรง 3D เหมาะกับใช้ในงานด้านวิศวกรรม https://www.freecadweb.org/

LibreCAD - ออกแบบรูปทรง 2D เหมาะกับใช้ในงานด้านวิศวกรรม https://librecad.org/

OpenFOAM - วิเคราะห์ปัญหาทางด้านวิศวกรรมด้านกลศาสตร์ของไหล https://openfoam.com/

KiCAD - ออกแบบวงจรอิเล็กทรอนิกส์ http://www.kicad-pcb.org/

Audacious, Lollypop, Elisa - เล่นเพลง https://audacious-media-player.org/ https://wiki.gnome.org/Apps/Lollypop https://community.kde.org/Elisa

Audacity - ตัดต่อเสียง https://www.audacityteam.org/

Gimp - ตัดต่อรูป https://gimp.org/

Krita, MyPaint - วาดรูป https://krita.org/en/ http://mypaint.org/

Inkscape - วาดเวกเตอร์ https://inkscape.org/

Synfig Studio - สร้างอนิเมชั่น 2D https://www.synfig.org/

OpenToonz - สร้างอนิเมชั่น 2D ที่สตูดิโอจิบลิใช้ https://opentoonz.github.io/

Kdenlive,OpenShot - ตัดต่อวิดีโอ https://kdenlive.org/ https://www.openshot.org/

Darktable, Rawtherapee - แต่งภาพ RAW https://www.darktable.org/ https://rawtherapee.com/

Digikam - บริหารจัดการรูปภาพ https://www.digikam.org/

PulseEffects - ปรับเสียง https://github.com/wwmm/pulseeffects

VLC - หนัง https://www.videolan.org/vlc/

Gwenview - ดูรูป https://userbase.kde.org/Gwenview

Spectacle (กด print screen) - แคปหน้าจอ https://kde.org/applications/graphics/spectacle/

VSCodium - โคดดิ้ง https://vscodium.com/

uGet - โปรแกรมช่วยโหลด https://ugetdm.com/

Deluge - โปรแกรมโหลด torrent https://www.deluge-torrent.org/

kSysGuard , htop - การใช้งาน CPU, RAM ฯลฯ https://userbase.kde.org/KSysGuard https://hisham.hm/htop/

kSystemLog - ดู log ของระบบ https://kde.org/applications/system/ksystemlog/

Handbrake - แปลงไฟล์ https://handbrake.fr/

OBS Studio - อัดหน้าจอ https://obsproject.com/

Scribus - คล้าย MS Publisher https://www.scribus.net/

Xournal - จดบันทึก https://sourceforge.net/projects/xournal/

Okular - ดู PDF https://okular.kde.org/

KeePassXC - จัดเก็บรหัสพ่าน https://keepassxc.org

SpeedCrunch - เครื่องคิดเลข https://speedcrunch.org

Kate - โน้ตแพด https://kde.org/applications/utilities/org.kde.kate

Ark - แตกไฟล์ https://kde.org/applications/utilities/org.kde.ark

Discord - บริการสื่อสารระหว่างเกมเมอร์ https://discordapp.com

Telegram - ส่งข้อความความปลอดภัยสูง https://telegram.org

Hardinfo - ดูข้อมูล hardware ในเครื่อง https://help.ubuntu.com/community/HardInfo

Virtual Machine Manager - สำหรับจำลองระบบคอมพิวเตอร์ https://virt-manager.org

RPCS3 - อีมูเลเตอร์ PlayStation 3 https://rpcs3.net

LMMS - โปรแกรมแต่งเสียง 	Digital audio workstation https://lmms.io

Ardour - โปรแกรมแต่งเสียง 	Digital audio workstation https://www.ardour.org/

Mixxx - โปรแกรมแต่งเสียง DJ https://www.mixxx.org/

MuseScore - โปรแกรมเขียนเพลง https://musescore.org/en

Tuxguitar - โปรแกรมสำหรับอ่านเขียน Tab http://tuxguitar.com.ar/ 

GNUCash - จัดการบัญชีส่วนบุคคล https://gnucash.org/

Skanlite - สแกนเอกสาร https://kde.org/applications/graphics/skanlite

Filezilla - ดาวน์โหลดหรืออัปโหลดไฟล์ https://filezilla-project.org/

Godot Engine - สร้างเกม https://godotengine.org/

KStars - ดูดาว https://kde.org/applications/education/kstars/

Marble - แผนที่โลก https://marble.kde.org/

Kalzium - ตารางธาตุ https://kde.org/applications/education/kalzium/

Avogadro - จำลองโมเลกุล https://avogadro.cc/

QGIS - จัดการข้อมูลสารสนเทศภูมิศาสตร์ https://www.qgis.org/en/site/

ImageJ - วิเคราะห์ภาพถ่าย https://imagej.net/Welcome

Octave, Scilab - ภาษาคอมพิวเตอร์ระดับสูงที่ใช้สำหรับคำนวณเชิงตัวเลข https://www.gnu.org/software/octave/ https://www.scilab.org/

Disks - ตรวจสอบประสิทธิภาพ Hard Disk https://wiki.gnome.org/Apps/Disks

Filelight - ดูพื้นที่ที่ใช้ใน hard disk https://kde.org/applications/utilities/org.kde.filelight

ImageWriter - สร้าง Live USB https://github.com/openSUSE/imagewriter

Webcamoid - เวปแคม https://webcamoid.github.io/

Labplot - สร้างกราฟและวิเคราะห์ข้อมูลทางวิทยาศาสตร์ https://labplot.kde.org/

OpenRA - เกมคล้าย Red Alert (https://www.openra.net/)

OpenTTD - เกมที่ได้แรงบันดาลใจจาก Transport Tycoon Deluxe https://www.openttd.org/

kmymoney - บริหารเงิน https://kmymoney.org/

Calibre - จัดการ e-book https://calibre-ebook.com/

Grsync - สำรองข้อมูล http://www.opbyte.it/grsync/

GNU Health - จัดการข้อมูลสำหรับโรงพยาบาล https://gnuhealth.org/

Netdata - ดูข้อมูลการใช้งาน https://www.netdata.cloud/

Kodi - จัดการภาพยนตร์รายการทีวีเพลงและภาพถ่าย https://kodi.tv/

Wireshark - ดักจับข้อมูล https://www.wireshark.org 
