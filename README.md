# KalaOS Edu  คืออะไร

กะลาโอเอส (KalaOS) เป็นระบบปฏิบัติการ Linux ครับ (เหมือนกับ Windows Android macOS อะไรพวกนี้) แต่ Linux เป็นระบบที่เปิดโคดให้ดูได้ฟรีและแจกจ่ายได้อิสระ เลยมี Linux หลายตัว

KalaOS เป็น Linux ทีผมดัดแปลงมาจาก OpenSUSE (https://www.opensuse.org/) โดยใช้ KIWI https://doc.opensuse.org/projects/kiwi/doc/

# สร้าง KalaOS มาทำไม

KalaOS นั้นไม่ได้ต่างกับ OpenSUSE มากนัก หลักๆก็มีแค่โปรแกรมและหน้าตาที่เพิ่มเข้ามา

แต่ KalaOS มีเป้าหมายอยู่ 2 อย่าง

* เป็นฐานให้ต่อยอด ให้ได้คนอื่นๆเข้าใจการสร้าง distro จาก KIWI ง่ายขึ้น
* ให้คนทั่วไปได้เลิกคิดว่า Linux มีโปรแกรมน้อย ใช้ทำงานทั่วไปลำบาก เลยยัดโปรแกรมมาเยอะๆ
* ตกแต่งหน้าตาให้สวยงามน่าใช้ ให้เลิกคิดว่า Linux ดูเก่าๆ ดูน่าเบื่อ

# Who maintain package and security patch in KalaOS?

Packages and security patches are from OpenSUSE Tumbleweed.

## เกี่ยวกับ KalaOS

* Based on OpenSUSE Tumbleweed 
* Package Manager - Zypper
* Desktop Environment - KDE Plasma 5
* Icon theme - Papirus
* Network manager - NetworkManager
* Default file system - EXT4

## ไฟล์ที่เห็นอยู่คืออะไร

ไฟล์ที่เห็นอยู่ (root, config.xml, config.sh) ไม่มีความสำคัญกับการใช้ KalaOS แต่มันคือไฟล์ที่ใช้สร้าง KalaOS ซึ่งเรียกว่า source code

root - ทุกอย่างที่ใส่ในนี้จะติดมากับถึงผู้ใช้ทั่วไป เหมาะกับการใส่ ไฟล์/การตั้งค่า/ธึม ให้ผู้ใช้

config.xml - ใส่โปรแกรมที่ต้องการ

config.sh - ใส่ key สำหรับ  repositories และ start/stop service และบอกว่าผู้ใช้คนไหนได้ login


ไฟล์ที่ใช้ติดตั้ง KalaOS คือ kalaos-kde1.iso

### สร้าง KalaOS จาก source code

ติดตั้ง KIWI (สำหรับ OpenSUSE)

```
sudo zypper in kiwi-tools python3-kiwi
```

ดาวน์โหลด config.sh, config.xml, root ไปไว้ในที่เดียวกัน

```
cd /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/
```
```
sudo kiwi-ng --type iso system build  --description /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/ --target-dir /ที่ไหนก็ได้/kalaos-kde1
```
### มีข้อผิดพลาด KiwiRootDirExists

```
[ ERROR   ]: 15:08:09 | KiwiRootDirExists: Root directory /ที่ไหนก็ได้/kalaos-kde1/build/image-root already exists
```
ให้เปลี่ยนชื่อเป็น kalaos-kde2

```
export TMPDIR=/home/ที่ไหนก็ได้/; sudo -E kiwi-ng --type iso system build  --description /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/ --target-dir /ที่ไหนก็ได้/kalaos-kde2
```

### มีข้อผิดพลาด KiwiInstallPhaseFailed

```
ERROR: 17:06:15 | KiwiInstallPhaseFailed: Systen package installation failed: No provider of 'Ardour' found
```
โปรแกรมที่ต้องการ (Ardour) ติดตั้งไม่อยู่ใน repository ที่ใส่ใว้ใน config.xml ให้ใส่ repository เพิ่มเข้าไป

```
<repository type="yast2" alias="MultiMedia" imageinclude="true" priority="100">
    <source path="https://download.opensuse.org/repositories/multimedia:/apps/openSUSE_Tumbleweed/"/>
    </repository>
```

### KalaOS มีขนาดเท่าไหรหลังสร้างเสร็จ 

ประมาณ 5.0 GiB.

### ติดตั้ง Live Image ลงบนคอมยังไง?

เราจะใช้ Calamares ในการติดตั้ง Live Image https://github.com/calamares/calamares
ให้ใส่ 
```
<package name="calamares"/> 
```
ใน config.xml

ใส่ dracut-kiwi-live-not-found.sh ไปใน /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/root/sbin (ดู source code เป็นตัวอย่าง)

ใส่ modules, qml, branding, settings.conf ของ calamares ไปใน /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/root/usr/share/calamares (ดู source code เป็นตัวอย่าง)

แก้ไข shellprocess.conf ใน /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/root/usr/share/calamares/modules โดยใส่ 

```
dontChroot: false
timeout: 10
script:

    - "usr/sbin/dracut-kiwi-live-not-found.sh"
```

ใน settings.conf ให้ใส่ - shellprocess ก่อน - dracut (ดู source code เป็นตัวอย่าง)

### คำเตือน

ถ้าใส่โปรแกรมไปใน config.xml เยอะมากๆ จะทำให้ kiwi-ng ใช้พื้นที่ใน /tmp เยอะมากๆ (KalaOS ใช้ ~15GiB) อาจทำให้ /root เต็มก่อนได้ 
แต่สามารถสั่งให้ kiwi-ng ไปใช้พื้นที่ใน /home/ที่ไหนก็ได้/ ชั่วคราวได้โดย

```
export TMPDIR=/home/ที่ไหนก็ได้/; sudo -E kiwi-ng --type iso system build  --description /ที่/ที่/เก็บไฟล์ที่โหลดเมื่อกี้/ --target-dir /ที่ไหนก็ได้/kalaos-kde1
```

# ซอร์ซโค้ดของ KalaOS ต้องทำอะไรบ้างให้ KalaOS ใช้งานได้ราบลื่น ? 

ซอร์ซโค้ดของ KalaOS มาจากการดัดแปลง https://github.com/OSInside/kiwi-descriptions

สิ่งที่ดัดแปลงหลักๆได้เขียนไว้ด้านล่าง

### KalaOS ทำอะไรให้ package manager ไม่อยากให้ถามหา key ทุกครั้ง 

ไปที่ repo ที่ต้องการ

ดาวน์โหลด .key หรอ .gpg จาก repo นั้นๆ.

ใส่ไปที่ /root/keys (ดู source code เป็นตัวอย่าง)

ดัดแปลง config.sh ให้มี key ของเราเช่น
```
rpm --import /keys/nvidia.key
```
(ดู source code เป็นตัวอย่าง)

### KalaOS ทำอะไรให้สามารถใช้อินเตอร์เน็ตได้?

ถ้าติดตั้ง NetworkManager ต้องเอา Wicked ออก

ใน config.sh เอา Wicked ออกด้วย 

```
baseRemoveService wicked
baseRemoveService wickedd
baseRemoveService wickedd-auto4
baseRemoveService wickedd-dhcp4
baseRemoveService wickedd-dhcp5
baseRemoveService wickedd-nanny
```

เปิดใช้งาน NetworkManager ด้วย

```
baseInsertService ModemManager
baseInsertService NetworkManager
```
(ดู source code เป็นตัวอย่าง)

### KalaOS ทำอะไรให้สามารถใช้อุปกรณ์อื่นๆที่ต่อกับคอมได้?

ถ้าไม่มี udev อะไรต่างๆที่เสียบไปในคอมจะไม่สามารถใช้งานได้ปกติ

ติดตั้ง udev ใน config.xml.

```
<package name="udev"/> 
```

### How KalaOS set a default theme?

KalaOS use Aritim theme as default, the theme can be manually installed in /root/home/tux/.local/share/plasma/ and /root/etc/skel/.local/share/plasma/ folder in KIWI descriptions. 

The theme is set by /root/home/tux/.config/plasmarc with "name=Aritim-Dark"

Default SDDM theme is Chili and can also be placed in /root/usr/share/sddm/themes/ with theme set by /root/etc/sddm.conf with "Current=plasma-chili"

Wallpaper config is here: /root/home/tux/.config/plasma-org.kde.plasma.desktop-appletsrc with wallpaper set by Image=file:///usr/share/wallpapers/KalaPenguin5brown.png

### อยากทำ ISO สำหรับ Ubuntu, Debian, Fedora, CentOS, Mageia

หา config.xml และ config.sh ได้ที่ 
```
https://github.com/OSInside/kiwi-descriptions
```
## Problems/Bugs/Softwarebugs/TODO

* How to properly transfer desktop theme to newly create user?

* Btrfs snapper integration on grub not support on Calamares at the moment

* Ardour need GTK2_RC_FILES variable  on desktop icon to start at the moment.

* Scilab Xcos is not support on Java 10 for OpenSUSE, replace with appimage at the moment. https://bugzilla.opensuse.org/show_bug.cgi?id=1153256 

* Karaoke, type writing software.

* Avatar icon for new user is generate with .icon and .icon.face from /etc/skel at the moment and doesn't take effect at first user creation, to change avatar, try to change avatar again.

## เขียนโดย Kevin Teenakul

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## ขอขอบคุณ

* schaefi (@schaefi:matrix.org)
* deivit (@deivit:matrix.org)
* Conan Kudo (@conan_kudo:matrix.org)
* defolos @defolos:matrix.org
* และหลายๆคนอีกมากมายที่ช่วยให้ KalaOS เกิดขึ้นมา

## โปรแกรมหลักๆที่มากับ KalaOS

รหัสผ่าน - 1234

เปลี่ยนภาษา - ปุ่มวินโดวส์ + spacebar

System Settings - ตั้งค่าหน้าตา 

YaST- ตั้งค่าระบบ http://yast.opensuse.org/

YaST Package Managment - ติดตั้งโปรแกรมขั้นสูง http://yast.opensuse.org/ 

Discover - ติดตั้งโปรแกรม https://userbase.kde.org/Discover

Firefox - เล่นเน็ต https://www.mozilla.org/
```
    -Ublock Origin
    -Decentraleye
    -Facebook Container
    -Uget Integration
    -Redirect AMP to HTML
    -DownThemAll
    -HTTPS Everywhere
    -Privacy Badger
```
LibreOffice - พิมพ์งาน https://www.libreoffice.org/
```
    -LibreOffice Writer - คล้าย MS Word
    -LibreOffice Impress - คล้าย MS PowerPoint
    -LibreOffice Calc - คล้าย MS Excel
    -LibreOffice Draw - คล้าย MS Visio
    -LibreOffice Base - คล้าย MS Access
```
Tor Browser - ใช้งานอินเตอร์เน็ตแบบนิรนามหรือการเข้า Deepweb https://www.torproject.org/

Blender - อนิเมชั่น 3D https://www.blender.org/

FreeCAD - ออกแบบรูปทรง 3D เหมาะกับใช้ในงานด้านวิศวกรรม https://www.freecadweb.org/

LibreCAD - ออกแบบรูปทรง 2D เหมาะกับใช้ในงานด้านวิศวกรรม https://librecad.org/

OpenFOAM - วิเคราะห์ปัญหาทางด้านวิศวกรรมด้านกลศาสตร์ของไหล https://openfoam.com/

KiCAD - ออกแบบวงจรอิเล็กทรอนิกส์ http://www.kicad-pcb.org/

Audacious, Lollypop, Elisa - เล่นเพลง https://audacious-media-player.org/ https://wiki.gnome.org/Apps/Lollypop https://community.kde.org/Elisa

Audacity - ตัดต่อเสียง https://www.audacityteam.org/

Gimp - ตัดต่อรูป https://gimp.org/

Krita, MyPaint - วาดรูป https://krita.org/en/ http://mypaint.org/

Inkscape - วาดเวกเตอร์ https://inkscape.org/

Synfig Studio - สร้างอนิเมชั่น 2D https://www.synfig.org/

OpenToonz - สร้างอนิเมชั่น 2D ที่สตูดิโอจิบลิใช้ https://opentoonz.github.io/

Kdenlive,OpenShot - ตัดต่อวิดีโอ https://kdenlive.org/ https://www.openshot.org/

Darktable, Rawtherapee - แต่งภาพ RAW https://www.darktable.org/ https://rawtherapee.com/

Digikam - บริหารจัดการรูปภาพ https://www.digikam.org/

PulseEffects - ปรับเสียง https://github.com/wwmm/pulseeffects

VLC - หนัง https://www.videolan.org/vlc/

Gwenview - ดูรูป https://userbase.kde.org/Gwenview

Spectacle (กด print screen) - แคปหน้าจอ https://kde.org/applications/graphics/spectacle/

VSCodium - โคดดิ้ง https://vscodium.com/

uGet - โปรแกรมช่วยโหลด https://ugetdm.com/

Deluge - โปรแกรมโหลด torrent https://www.deluge-torrent.org/

kSysGuard , htop - การใช้งาน CPU, RAM ฯลฯ https://userbase.kde.org/KSysGuard https://hisham.hm/htop/

kSystemLog - ดู log ของระบบ https://kde.org/applications/system/ksystemlog/

Handbrake - แปลงไฟล์ https://handbrake.fr/

OBS Studio - อัดหน้าจอ https://obsproject.com/

Scribus - คล้าย MS Publisher https://www.scribus.net/

Xournal - จดบันทึก https://sourceforge.net/projects/xournal/

Okular - ดู PDF https://okular.kde.org/

KeePassXC - จัดเก็บรหัสพ่าน https://keepassxc.org

SpeedCrunch - เครื่องคิดเลข https://speedcrunch.org

Kate - โน้ตแพด https://kde.org/applications/utilities/org.kde.kate

Ark - แตกไฟล์ https://kde.org/applications/utilities/org.kde.ark

Hardinfo - ดูข้อมูล hardware ในเครื่อง https://help.ubuntu.com/community/HardInfo

Virtual Machine Manager - สำหรับจำลองระบบคอมพิวเตอร์ https://virt-manager.org

LMMS - โปรแกรมแต่งเสียง 	Digital audio workstation https://lmms.io

Ardour - โปรแกรมแต่งเสียง 	Digital audio workstation https://www.ardour.org/

Mixxx - โปรแกรมแต่งเสียง DJ https://www.mixxx.org/

MuseScore - โปรแกรมเขียนเพลง https://musescore.org/en

Tuxguitar - โปรแกรมสำหรับอ่านเขียน Tab http://tuxguitar.com.ar/ 

GNUCash - จัดการบัญชีส่วนบุคคล https://gnucash.org/

Skanlite - สแกนเอกสาร https://kde.org/applications/graphics/skanlite

Filezilla - ดาวน์โหลดหรืออัปโหลดไฟล์ https://filezilla-project.org/

Godot Engine - สร้างเกม https://godotengine.org/

KStars - ดูดาว https://kde.org/applications/education/kstars/

Marble - แผนที่โลก https://marble.kde.org/

Kalzium - ตารางธาตุ https://kde.org/applications/education/kalzium/

Avogadro - จำลองโมเลกุล https://avogadro.cc/

QGIS - จัดการข้อมูลสารสนเทศภูมิศาสตร์ https://www.qgis.org/en/site/

ImageJ - วิเคราะห์ภาพถ่าย https://imagej.net/Welcome

Octave, Scilab - ภาษาคอมพิวเตอร์ระดับสูงที่ใช้สำหรับคำนวณเชิงตัวเลข https://www.gnu.org/software/octave/ https://www.scilab.org/

Disks - ตรวจสอบประสิทธิภาพ Hard Disk https://wiki.gnome.org/Apps/Disks

Filelight - ดูพื้นที่ที่ใช้ใน hard disk https://kde.org/applications/utilities/org.kde.filelight

ImageWriter - สร้าง Live USB https://github.com/openSUSE/imagewriter

Webcamoid - เวปแคม https://webcamoid.github.io/

Labplot - สร้างกราฟและวิเคราะห์ข้อมูลทางวิทยาศาสตร์ https://labplot.kde.org/

kmymoney - บริหารเงิน https://kmymoney.org/

Calibre - จัดการ e-book https://calibre-ebook.com/

Grsync - สำรองข้อมูล http://www.opbyte.it/grsync/

GNU Health - จัดการข้อมูลสำหรับโรงพยาบาล https://gnuhealth.org/

Netdata - ดูข้อมูลการใช้งาน https://www.netdata.cloud/

Kodi - จัดการภาพยนตร์รายการทีวีเพลงและภาพถ่าย https://kodi.tv/

Wireshark - ดักจับข้อมูล https://www.wireshark.org

TIPP10 - ฝึกพิมพ์ดีด https://www.tipp10.com/en/

Scratch

openMVG - โปรแกรมโฟโตแกรมเมตรี สร้างโมเดล 3D จากรูปภาพ

openMVS - โปรแกรมโฟโตแกรมเมตรี สร้างโมเดล 3D จากรูปภาพ ใช้คู่กับ openMVG

Meshlab - โปรแกรมสร้างแบบจำลอง 3 มิติ สร้างโมเดล 3D จากรูปภาพ

Cantor

lyx

kile
