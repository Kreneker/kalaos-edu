#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$kiwi_iname]..."

#======================================
# Mount system filesystems
#--------------------------------------
baseMount

#======================================
# Setup baseproduct link
#--------------------------------------
suseSetupProduct

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey 
rpm --import /keys/codium.gpg
rpm --import /keys/education.key
rpm --import /keys/games.key
rpm --import /keys/geo.key
rpm --import /keys/graphics.key
rpm --import /keys/M17N.key
rpm --import /keys/main_update.key
rpm --import /keys/main.key
rpm --import /keys/multimedia.key
rpm --import /keys/nonoss_update.key
rpm --import /keys/nonoss.key
rpm --import /keys/nvidia.key
rpm --import /keys/packman.key
rpm --import /keys/science.key
#======================================
# Disable services
#--------------------------------------
baseRemoveService wicked
baseRemoveService wickedd
baseRemoveService wickedd-auto4
baseRemoveService wickedd-dhcp4
baseRemoveService wickedd-dhcp5
baseRemoveService wickedd-nanny
#======================================
# Activate services
#--------------------------------------
suseInsertService sshd
baseInsertService apparmor
baseInsertService appstream-sync-cache
baseInsertService auditd
baseInsertService avahi-daemon
baseInsertService bluetooth
baseInsertService cron
baseInsertService display-manager
baseInsertService firewalld
baseInsertService getty@tty1
baseInsertService haveged
baseInsertService irqbalance
baseInsertService iscsi
baseInsertService kbdsettings
baseInsertService mcelog
baseInsertService ModemManager
baseInsertService NetworkManager
baseInsertService NetworkManager-dispatcher
baseInsertService NetworkManager-wait-online
baseInsertService nscd
baseInsertService purge-kernels
baseInsertService smartd
baseInsertService systemd-remount-fs
baseInsertService systemd-timesyncd
baseInsertService YaST2-Firstboot
baseInsertService YaST2-Second-Stage

#======================================
# Setup default target, multi-user
#--------------------------------------

#==========================================
# remove package docs
#------------------------------------------
rm -rf /usr/share/doc/packages/*
rm -rf /usr/share/doc/manual/*
rm -rf /opt/kde*

#==========================================
# configure and install flatpak apps
#------------------------------------------
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install -y -vv --ostree-verbose --noninteractive flathub com.microsoft.Teams
flatpak install -y -vv --ostree-verbose --noninteractive flathub com.slack.Slack

#======================================
# only basic version of vim is
# installed; no syntax highlighting
#--------------------------------------
sed -i -e's/^syntax on/" syntax on/' /etc/vimrc

#======================================
# SuSEconfig
#--------------------------------------
baseUpdateSysConfig /etc/sysconfig/windowmanager DEFAULT_WM plasma5
baseUpdateSysConfig /etc/sysconfig/displaymanager DISPLAYMANAGER sddm
baseUpdateSysConfig /etc/sysconfig/displaymanager DISPLAYMANAGER_AUTOLOGIN tux
baseSetRunlevel 5
suseConfig

#======================================
# Remove yast if not in use
#--------------------------------------


#======================================
# Umount kernel filesystems
#--------------------------------------
baseCleanMount

exit 0
